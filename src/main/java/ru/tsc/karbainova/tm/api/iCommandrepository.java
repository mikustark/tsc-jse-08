package ru.tsc.karbainova.tm.api;

import ru.tsc.karbainova.tm.model.Command;

public interface iCommandrepository {
    Command[] getTerminalCommands();
}
